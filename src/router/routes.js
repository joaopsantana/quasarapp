
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/mapaFeira', component: () => import('pages/mapaFeira.vue') },
      { path: '/favoritos', component: () => import('pages/favoritos.vue') },
      { path: '/historico', component: () => import('pages/historico.vue') },
      { path: '/conta', component: () => import('pages/conta.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
